<?
$db = require( __DIR__ . '/db.php' );

return [
	'bootstrap' => ['gii'],
	'modules' => [
		'gii' => 'yii\gii\Module'
	],
	'components' => [
		'db' => $db
	]
];
