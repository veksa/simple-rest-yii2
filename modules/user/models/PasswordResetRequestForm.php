<?
namespace app\modules\user\models;

use yii\base\Model;

class PasswordResetRequestForm extends Model{
	public $email;

	public function attributeLabels(){
		return [
			'email' => 'E-mail'
		];
	}

	public function rules(){
		return [
			[
				'email',
				'filter',
				'filter' => 'trim'
			],
			[
				'email',
				'required',
				'message' => 'Обязательное поле'
			],
			[
				'email',
				'email',
				'message' => 'Неверный формат'
			],
			[
				'email',
				'exist',
				'targetClass' => '\app\modules\user\models\User',
				'filter' => [ 'status' => User::STATUS_ACTIVE ],
				'message' => 'Пользователь с указанным e-mail не найден.'
			]
		];
	}

	public function sendEmail(){
		$user = User::findOne( [
			'status' => User::STATUS_ACTIVE,
			'email' => $this->email
		] );

		if( $user ){
			if( !User::isPasswordResetTokenValid( $user->password_reset_token ) ){
				$user->generatePasswordResetToken();
			}

			if( $user->save() ){
				return \Yii::$app->mailer->compose( [
					'html' => 'passwordResetToken-html',
					'text' => 'passwordResetToken-text'
				], [ 'user' => $user ] )
					->setFrom( [ \Yii::$app->params['noreplyEmail'] => \Yii::$app->name . ' robot' ] )
					->setTo( $this->email )
					->setSubject( 'Восстановление пароля для ' . \Yii::$app->name )
					->send();
			}
		}

		return false;
	}
}
