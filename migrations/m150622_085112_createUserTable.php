<?
use yii\db\Schema;
use yii\db\Migration;

class m150622_085112_createUserTable extends Migration{
	public function up(){
		$tableOptions = NULL;
		if( $this->db->driverName === 'mysql' ){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable( '{{%user}}', [
			'id' => Schema::TYPE_PK,
			'username' => Schema::TYPE_STRING . ' NOT NULL',
			'email' => Schema::TYPE_STRING . ' NOT NULL',
			'auth_key' => Schema::TYPE_STRING . '(32) NULL DEFAULT NULL',
			'email_confirm_token' => Schema::TYPE_STRING . ' NULL DEFAULT NULL',
			'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
			'password_reset_token' => Schema::TYPE_STRING . ' NULL DEFAULT NULL',
			'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
			'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
			'password' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->createIndex( 'idx_user_username', '{{%user}}', 'username' );
		$this->createIndex( 'idx_user_email', '{{%user}}', 'email' );
		$this->createIndex( 'idx_user_status', '{{%user}}', 'status' );
	}

	public function down(){
		$this->dropTable( '{{%user}}' );
	}
}
